import { Field, ArgsType } from '@nestjs/graphql';
import { IsArray } from 'class-validator';

@ArgsType()
export class GetMemosArgs {
    @Field(() => [String])
    @IsArray()
    memoIds: string[];
}