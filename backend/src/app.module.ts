import { Module } from '@nestjs/common';
// import { AppController } from './app.controller';
// import { AppService } from './app.service';
import { Neo4jModule } from 'nest-neo4j';
import { HOSTNAME, NEO4J_USER, NEO4J_PASSWORD, NEO4J_DATABASE } from './config';
// import { GraphqlResolver } from './graphql/graphql.resolver';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { EncryptionModule } from './encryption/encryption.module';
import { ConfigModule } from '@nestjs/config';
import { MemoModule } from './memo/memo.module';
@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      context: ({ req }) => ({ req }),
      definitions: {
        path: join(process.cwd(), 'src/graphql.ts'),
        outputAs: 'class',
      },
    }),
    ConfigModule.forRoot({ isGlobal: true }),
    Neo4jModule.forRoot({
      scheme: 'neo4j',
      host: HOSTNAME,
      port: 7687,
      username: NEO4J_USER,
      password: NEO4J_PASSWORD,
      database: NEO4J_DATABASE
    }),
    UserModule,
    AuthModule,
    EncryptionModule,
    MemoModule,
  ],
})
export class AppModule {}
