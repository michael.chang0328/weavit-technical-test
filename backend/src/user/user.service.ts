import { Injectable } from '@nestjs/common';
import { Neo4jService } from '../neo4j/neo4j.service';
import { EncryptionService } from '../encryption/encryption.service';
import { User } from './models/user';
import { CreateUserInput } from './dto/input/create-user.input';
import { v4 as uuidv4} from 'uuid';
import { UpdateUserInput } from './dto/input/update-user.input';
import { DeleteUserInput } from './dto/input/delete-user.input';

@Injectable()
export class UserService {

    constructor(private readonly neo4jService: Neo4jService, private readonly encryptionService: EncryptionService) { }


    async findUserByEmail(email: string): Promise<User | undefined> {
        const result = await this.neo4jService.read(`
            MATCH (u:User {email: $email})
            RETURN u
        `,{ email }) 
        return result.records.length == 1 ? result.records[0].get('u').properties : undefined
    }

    async findUserById(userId: string): Promise<User | undefined> {
        const result = await this.neo4jService.read(`
            MATCH (u:User {userId: $userId})
            RETURN u
        `,{ userId }) 
        return result.records.length == 1 ? result.records[0].get('u').properties : undefined
    }

    // async createUser(email: string, password: string, firstName: string, lastName: string): Promise<User> {
    //     const res = await this.neo4jService.write(`CREATE (u:User) SET u += $properties, u.id = randomUUID(), u.created_at = datetime(), u.updated_at = datetime() RETURN u`, {
    //         properties: {
    //             email, password: await this.encryptionService.hash(password), firstName, lastName,
    //         },
    //     })

    //     return res.records[0].get('u');
    // }

    async getUser() {
        
    }

    async createUser(createUserInput: CreateUserInput): Promise<User> {
        const user: User = {
            userId: uuidv4(),
            created_at: new Date(),
            updated_at: new Date(),
            password: await this.encryptionService.hash(createUserInput.password),
            ...createUserInput
        }
        const res = await this.neo4jService.write(`CREATE (u:User) SET u += $properties RETURN u`, {
            properties: {
                ...user
            }
        })
        return res.records[0].get('u').properties;
    }

    async getAllUsers(): Promise<User>{
        const users = await this.neo4jService.read(`MATCH (u:User) RETURN u`)

        return users.records[0].get('u').properties;
    }

    async updateUserTime(updateUserInput: UpdateUserInput): Promise<User> {
        const findUser = await this.findUserByEmail(updateUserInput.userId);

        const updatedInfo = await this.neo4jService.write(`MATCH (u:User {userId: $userId}) SET u += $properties RETURN u`, {
            properties: {
                updated_at: new Date(),
                ...findUser
            }
        })
        return updatedInfo.records[0].get('u').properties;
    }

    async deleteUser(deleteUserInput: DeleteUserInput): Promise<User> {

        const deleteUserById = await this.neo4jService.write(`MATCH (u:User) SET u += $properties DELETE u`, {
            properties: {
                ...deleteUserInput
            }
        })

        return deleteUserById.records[0].get('u').properties;
    }

}
