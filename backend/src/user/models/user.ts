import { ObjectType, Field } from "@nestjs/graphql";
import { IsUUID, IsOptional } from "class-validator";


@ObjectType()
export class User {
    @Field()
    @IsUUID()
    @IsOptional()
    userId?: string;
    @Field()
    email: string;
    @Field()
    password: string;
    @Field()
    firstName: string;
    @Field()
    lastName: string;
    @Field()
    created_at?: Date;
    @Field()
    updated_at?: Date;
}