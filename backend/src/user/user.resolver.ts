import { Query, Resolver, Mutation, Args } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User } from './models/user';
import { GetUserArgs } from './dto/args/get-user.args';
import { GetUsersArgs } from './dto/args/get-users.args';
import { CreateUserInput } from './dto/input/create-user.input';
import { UpdateUserInput } from './dto/input/update-user.input';
import { DeleteUserInput } from './dto/input/delete-user.input';
import { AuthService } from '../auth/auth.service';
import { Jwt } from '../auth/models/jwt';


@Resolver(() => User)
export class UserResolver {
    constructor(private readonly userService: UserService, private readonly authService: AuthService) { }

    @Query(() => User, { name: 'user', nullable: true } )
    async getUser(@Args() getUserArgs: GetUserArgs): Promise<User> {
        return await this.userService.findUserById(getUserArgs.userId);
    }

    @Query(() => [User], { name: 'users', nullable: 'items' } )
    async getAllUsers(@Args() getUsersArgs: GetUsersArgs) {
        return await this.userService.getAllUsers();
    }

    // @Mutation(() => Number)
    // getNum(@Args('num') num: number) {
    //     console.log(num);
    //     return num;
    // }
    @Mutation(() => Jwt)
    async createUser(@Args('createUser') user: CreateUserInput) {
        const res = await this.userService.createUser(user);
        const jwt = await this.authService.createToken(res);
        return jwt;
    }

    @Mutation(() => User)
    async updateUser(@Args('updateUser') user: UpdateUserInput): Promise<User> {
        const res = await this.userService.updateUserTime(user)
        return res;
    }

    @Mutation(() => User)
    async deleteUser(@Args('deleteUser') user: DeleteUserInput): Promise<User> {
        const res = await this.userService.deleteUser(user)
        return res;
    }
    
}